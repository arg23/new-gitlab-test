import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-npm-test-lib',
  template: `
    <p>
      npm-test-lib works!
    </p>
  `,
  styles: [
  ]
})
export class NpmTestLibComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
