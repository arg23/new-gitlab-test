import { TestBed } from '@angular/core/testing';

import { NpmTestLibService } from './npm-test-lib.service';

describe('NpmTestLibService', () => {
  let service: NpmTestLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NpmTestLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
