import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NpmTestLibComponent } from './npm-test-lib.component';

describe('NpmTestLibComponent', () => {
  let component: NpmTestLibComponent;
  let fixture: ComponentFixture<NpmTestLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NpmTestLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NpmTestLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
