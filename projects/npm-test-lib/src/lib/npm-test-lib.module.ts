import { NgModule } from '@angular/core';
import { NpmTestLibComponent } from './npm-test-lib.component';



@NgModule({
  declarations: [NpmTestLibComponent],
  imports: [
  ],
  exports: [NpmTestLibComponent]
})
export class NpmTestLibModule { }
