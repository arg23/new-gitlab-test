/*
 * Public API Surface of npm-test-lib
 */

export * from './lib/npm-test-lib.service';
export * from './lib/npm-test-lib.component';
export * from './lib/npm-test-lib.module';
